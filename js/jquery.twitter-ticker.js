/*
 * Simple Twitter Typing Ticker Plugin
 * Version: 0.1
 * Author: Josh Schrade
 * 
 * Based off of Highly configurable' mutable plugin boilerplate
 * By: @markdalgleish
 */

/* Usages
 *Dom setup:
 *<div class=".twitter-ticker"></div>
 *
 *$('.twitter-ticker').twitterTicker();
 * var ticker = $('.twitter-ticker').data('ticker');
 *
 *or:
 *
 *var ticker = new TwitterTicker($(".twitter-ticker"), {}).init();
 *
 *or with config pased in via data attribute:
 *<div class=".twitter-ticker" data-ticker-options='{"screen_name":"acedevs",'count":3}'></div>
 *$('.twitter-ticker').twitterTicker();
 */

;(function( $, window, document, undefined ){

  var TwitterTicker = function( elem, options ){
      this.format = 'json';
      this.endpoint = '//api.twitter.com/1/statuses/user_timeline';
      this.twitter_page = 'https://twitter.com';
      this.tick_interval = null;
      this.display_interval = null;
      this.display_index = 0;
      this.tweets = null;
      this.tweets_index = 0;
      this.elem = elem;
      this.$elem = $(elem);
      this.options = options;
      this.metadata = this.$elem.data( 'ticker-options' );
    };

  TwitterTicker.prototype = {
    defaults: {
      screen_name: 'jquery',
      interval: 5000,
      display: 20,
      trim_user: true,
      count: 10,
      exclude_replies:true,
      forward_onclick:true,
      redirect:false
    },

    init: function() {
      this.config = $.extend({}, this.defaults, this.options, 
      this.metadata);
      if (this.config.forward_onclick){
        var forward_to = this.twitter_page+"/"+this.config.screen_name;
        if (this.config.redirect){
            this.$elem.click(function(){window.location=forward_to;});
        } else {
            this.$elem.click(function(){window.open(forward_to);});
        };
      };
      this.reload();
      return this;
    },

    reload: function() {
        this.stop();
        this.tweets_index = 0;
        var full_endpoint = 'http:'+this.endpoint+'.'+this.format;
        var this_ref = this;
        $.ajax({
            url: full_endpoint,
            dataType: 'jsonp',
            data: {
                'screen_name':this.config.screen_name,
                'trim_user':this.config.trim_user,
                'count':this.config.count
            },
            success: function (data){
                this_ref.tweets = data;
                this_ref.stop();
                this_ref.tick();
                this_ref.start();
            }
        });
    },
    
    tick: function() {
      clearInterval(this.display_interval);
      this.display_index = 0;
      this.$elem.text('');
      var tweet = this.tweets[this.tweets_index];
      if (typeof(tweet) == 'undefined'){
        this.tweets_index = 0;
        tweet = this.tweets[this.tweets_index];
      };
      this.tweets_index += 1;
      var this_ref = this;
      var parts = tweet.text.split('');
      this.display_interval = setInterval(function(){
        var display_char = parts[this_ref.display_index];
        if (typeof(display_char) != 'undefined'){
            this_ref.display_index += 1;
            this_ref.$elem.append(display_char);
        } else {
            clearInterval(this_ref.display_interval);
            this_ref.display_index = 0;
        };
      }, this.config.display);
    },
    
    start: function() {
        this.stop();
        this.tick();
        if (this.tweets != null){
            var this_ref = this;
            this.tick_interval = setInterval(function(){
                this_ref.tick();
            }, this.config.interval)
        } else {
            this.reload();
        };
    },
    
    stop: function() {
        clearInterval(this.tick_interval);
    }
    
  }

  TwitterTicker.defaults = TwitterTicker.prototype.defaults;

  $.fn.twitterTicker = function(options) {
    return this.each(function() {
        if (!$.data(this, 'ticker')){
            $.data(this, 'ticker', new TwitterTicker(this, options).init());
        };
    });
  };

  window.TwitterTicker = TwitterTicker;

})( jQuery, window , document );
